FROM openjdk:11-jre-slim-buster
EXPOSE 8083
ARG JAR_FILE=target/authentication-1.0.0.jar
ADD ${JAR_FILE} authentication.jar
ENTRYPOINT ["java","-jar","/authentication.jar"]