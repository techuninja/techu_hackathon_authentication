package com.techu.hackathon.authentication.repository;

import com.techu.hackathon.authentication.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    User findByDni(String username);
}
