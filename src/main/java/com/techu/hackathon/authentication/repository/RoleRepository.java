package com.techu.hackathon.authentication.repository;

import com.techu.hackathon.authentication.model.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoleRepository extends MongoRepository<Role, String> {

    Role findByRole(String role);
}
