package com.techu.hackathon.authentication.controller;

import com.techu.hackathon.authentication.configuration.JwtTokenProvider;
import com.techu.hackathon.authentication.dto.AuthBody;
import com.techu.hackathon.authentication.dto.UserResponse;
import com.techu.hackathon.authentication.repository.UserRepository;
import com.techu.hackathon.authentication.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping("/authentication")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    UserRepository users;

    @Autowired
    private CustomUserDetailsService userService;

    @SuppressWarnings("rawtypes")
    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponse> login(@RequestBody AuthBody data) {
        try {
            String username = data.getUsername();
            //userService.saveUser(data);
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, data.getPassword()));
            String token = jwtTokenProvider.createToken(username, this.users.findByDni(username).getRoles());
            UserResponse userResponse = userService.findUserByDni(username, token);
            return new ResponseEntity<>(userResponse, HttpStatus.OK);
            //return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Credenciales invalidas");
        }
    }
}
