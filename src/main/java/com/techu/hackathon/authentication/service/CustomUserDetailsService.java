package com.techu.hackathon.authentication.service;

import com.techu.hackathon.authentication.dto.AuthBody;
import com.techu.hackathon.authentication.dto.UserResponse;
import com.techu.hackathon.authentication.model.Role;
import com.techu.hackathon.authentication.model.User;
import com.techu.hackathon.authentication.repository.RoleRepository;
import com.techu.hackathon.authentication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    public UserResponse findUserByDni(String username, String token) {
        User user = userRepository.findByDni(username);
        return new UserResponse(username, user.getCodigoCliente(),
                user.getNombre(), user.getApellido(), user.getFechaNacimiento(), token);
    }

    public void saveUser(AuthBody userAuth) {
        User user = new User();
        user.setDni(userAuth.getUsername());
        user.setPassword(userAuth.getPassword());
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setCodigoCliente("11223344");
        user.setNombre("Henry");
        user.setApellido("Soto");
        user.setFechaNacimiento("1990-08-01");
        user.setEnabled(true);
        Role userRole = roleRepository.findByRole("CUSTOMER");
        user.setRoles(new HashSet<>(Arrays.asList(userRole)));
        userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByDni(username);
        if(user != null) {
            List<GrantedAuthority> authorities = getUserAuthority(user.getRoles());
            return buildUserForAuthentication(user, authorities);
        } else {
            throw new UsernameNotFoundException("User not found");
        }
    }

    private List<GrantedAuthority> getUserAuthority(Set<Role> userRoles) {
        Set<GrantedAuthority> roles = new HashSet<>();
        userRoles.forEach((role) -> {
            roles.add(new SimpleGrantedAuthority(role.getRole()));
        });

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>(roles);
        return grantedAuthorities;
    }

    private UserDetails buildUserForAuthentication(User user, List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(user.getDni(),
                user.getPassword(), authorities);
    }
}
